package com.himanshuthings.dev.thingsswitch;

import android.content.Context;

/**
 * Created by himanshubaghel on 08/12/17.
 */

public class PreferenceManager {

    public static final String PREFS_FILE_NAME = "hb.thingsswitch_prefs";

    public static final String IN_IT_TIME = "IN_IT_TIME";
    public static final String SWITCHED_ON_AT = "SWITCHED_ON_AT";

    public static void putString(Context context, String key, String value) {
        context.getSharedPreferences(PREFS_FILE_NAME, 0).edit().putString(key, value).commit();
    }

    public static String getString(Context context, String key, String defValue) {
        return context.getSharedPreferences(PREFS_FILE_NAME, 0).getString(key, defValue);
    }

    public static void putLong(Context context, String key, long time) {
        context.getSharedPreferences(PREFS_FILE_NAME, 0).edit().putLong(key, time).commit();
    }

    public static long getLong(Context context, String key, long defValue) {
        return context.getSharedPreferences(PREFS_FILE_NAME, 0).getLong(key, defValue);
    }

    public static void inItTime(Context context, long time) {
        putLong(context, IN_IT_TIME, time);
    }

    public static long getInitialTime(Context context) {
        return getLong(context, IN_IT_TIME, 0);
    }

    public static void setSwitchedOnAt(Context context, long time) {
        putLong(context, SWITCHED_ON_AT, time);
    }

    public static long getSwitchedOnAt(Context context) {
        return getLong(context, SWITCHED_ON_AT, 0);
    }
}
