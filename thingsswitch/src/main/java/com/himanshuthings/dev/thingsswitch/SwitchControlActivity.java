package com.himanshuthings.dev.thingsswitch;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.things.pio.Gpio;
import com.google.android.things.pio.PeripheralManagerService;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.himanshuthings.dev.thingsswitch.pirsensor.MotionSensor;
import com.himanshuthings.dev.thingsswitch.pirsensor.PirMotionSensor;

import java.io.IOException;
import java.util.ArrayList;

public class SwitchControlActivity extends AppCompatActivity implements MotionSensor.Listener{

    public static final String TAG = SwitchControlActivity.class.getSimpleName();

    private ArrayList<Switch> mSwitches = new ArrayList<>();
    private SwitchesAdapter mAdapter;

    private DatabaseReference mDatabaseRef;
    TextView mIpAddressTextView;
    private PirMotionSensor mMotionSensor;
    public static long MAX_WAIT_FOR_MOTION = 10 * 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_switch_control);

        //Initialize PeripheralManagerService
        PeripheralManagerService service = new PeripheralManagerService();

        //Check for the available GPIOs
        if (service.getGpioList().isEmpty()) {
            Log.e(TAG, "No GPIO port available on this device.");
            finish();
            return;
        }

        mIpAddressTextView = (TextView) findViewById(R.id.ip_address);

        mDatabaseRef = FirebaseDatabase.getInstance().getReference().child(Constant.REF_NAME);

        updateIPAddress();
        initMotionSensor();
        mDatabaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (Switch s : mSwitches) {
                    Switch dataSwitch = dataSnapshot.child(s.getName()).getValue(Switch.class);
                    Log.i(TAG,dataSwitch.getName()+":"+dataSwitch.getStatus());
                    //Check changed pin by the pin name
                    if (s.getName().equals(dataSwitch.getName())) {
                        try {
                            if (s.getName().equalsIgnoreCase(Constant.LIGHT_BULB_PIN)) {
                                updateLightSwitch(dataSwitch.getStatus(), false);
                                updateIPAddress();
                            } else {
                                s.setStatus(dataSwitch.getStatus());
                            }
                            s.setIgnoreForMinutes(dataSwitch.getIgnoreForMinutes());
                            PreferenceManager.inItTime(SwitchControlActivity.this, System.currentTimeMillis());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                //Refresh the list
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //Set the BCM6 port as the GPIO for all pins.
        try {
            mSwitches = Switch.getSwitches(service, getApplicationContext());
            mAdapter = new SwitchesAdapter(this, mSwitches, mDatabaseRef);
            ListView recyclerView = (ListView) findViewById(R.id.switches_list);

            recyclerView.setAdapter(mAdapter);
        } catch (IOException e) {
            e.printStackTrace();
        }

        checkEverySecond();
    }


    private Gpio openMotionSensorGpioBus() {
        Gpio bus;
        try {
            bus = new PeripheralManagerService().openGpio(BoardDefaults.getGPIOForPIRSensor());
        } catch (IOException e) {
            throw new IllegalStateException("Can't open GPIO - can't create app.", e);
        }
        return bus;
    }

    void initMotionSensor() {
        Gpio bus = openMotionSensorGpioBus();
        mMotionSensor = new PirMotionSensor(bus, this);
        mMotionSensor.startup();
    }

    @Override
    public void onMovement() {
        if (!shouldIgnore()){
            updateLightSwitch(true, true);
        }

    }

    void updateLightSwitch(boolean status, boolean syncDatabase) {
        Switch s = getLightSwitch();
        if (s.getStatus() == status) {
            return;
        }
        try {
            s.setStatus(status);
            if (syncDatabase) {
                mDatabaseRef.child(s.getName()).setValue(s);
            }
            mAdapter.notifyDataSetChanged();
            PreferenceManager.setSwitchedOnAt(SwitchControlActivity.this, System.currentTimeMillis());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    boolean shouldIgnore() {
        Switch s = getLightSwitch();
        if (s != null) {
            long timeToIgnore = s.getIgnoreForMinutes() * 60 * 1000;
            return (System.currentTimeMillis() - PreferenceManager.getInitialTime(
                    SwitchControlActivity.this)) < timeToIgnore;
        }
        return true;
    }

    void checkEverySecond() {
        final Handler handler = new Handler();
        final int delay = 1500; //milliseconds

        handler.postDelayed(new Runnable(){
            public void run(){
                //do something
                if (System.currentTimeMillis() - PreferenceManager.getSwitchedOnAt(
                        SwitchControlActivity.this) > MAX_WAIT_FOR_MOTION) {
                    if (!shouldIgnore()) {
                        updateLightSwitch(false, true);
                    }
                }

                handler.postDelayed(this, delay);
            }
        }, delay);
    }

    Switch getLightSwitch() {
        for (Switch s : mSwitches) {
            if (s.getName().equalsIgnoreCase(Constant.LIGHT_BULB_PIN))
                return s;
        }
        return null;
    }

    @Override
    protected void onDestroy() {
        mMotionSensor.shutdown();
        super.onDestroy();
    }

    void updateIPAddress() {
        String ipAddress = UtilMethods.getIPAddress(true);
        DatabaseReference switchRef = mDatabaseRef.child("IP_ADDRESS").getRef();
        switchRef.setValue(ipAddress);
        mIpAddressTextView.setText("IpAddress : " + ipAddress);
        Log.i(TAG, "IP Address : " + ipAddress);
    }
}
