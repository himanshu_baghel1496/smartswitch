package com.himanshuthings.dev.thingsswitch;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

import com.google.firebase.database.DatabaseReference;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Keval on 27-Apr-17.
 */

public class SwitchesAdapter extends ArrayAdapter {

    private Context mContext;
    private ArrayList<Switch> mSwitches;
    private DatabaseReference mDatabaseReference;

    public SwitchesAdapter(@NonNull Context context, @LayoutRes int resource) {
        super(context, resource);
    }

    public SwitchesAdapter(Context context, ArrayList<Switch> switches, DatabaseReference reference) {
        super(context, -1);
        mContext = context;
        mSwitches = switches;
        mDatabaseReference = reference;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        convertView = LayoutInflater.from(mContext).inflate(R.layout.item_switch, parent, false);
        SwitchViewHolder holder = new SwitchViewHolder(convertView);
        final Switch switch1 = mSwitches.get(position);

        holder.mSwitchTextView.setText(switch1.getName().toUpperCase().replace("LIGHTBULB", "LIGHT BULB"));

        holder.mSwitchToggel.setBackgroundResource(R.drawable.bulb_selector);
        holder.mSwitchToggel.setOnCheckedChangeListener(null);
        holder.mSwitchToggel.setChecked(switch1.getStatus());
        holder.mSwitchToggel.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                try {
                    switch1.setStatus(isChecked);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                DatabaseReference switchRef = mDatabaseReference.child(switch1.getName()).getRef();
                switchRef.setValue(switch1);

                notifyDataSetChanged();
            }
        });
        return convertView;
    }

    @Override
    public int getCount() {
        return mSwitches.size();
    }


    public class SwitchViewHolder {
        private AppCompatTextView mSwitchTextView;
        private ToggleButton mSwitchToggel;

        public SwitchViewHolder(View itemView) {

            mSwitchTextView = (AppCompatTextView) itemView.findViewById(R.id.switch_name_tv);
            mSwitchToggel = (ToggleButton) itemView.findViewById(R.id.switch_status_toggel);

            ViewGroup.LayoutParams params = mSwitchToggel.getLayoutParams();
            int square = Resources.getSystem().getDisplayMetrics().widthPixels / 2;
            params.width = square;
            params.height = square - 50;
            mSwitchToggel.setLayoutParams(params);

        }
    }
}
