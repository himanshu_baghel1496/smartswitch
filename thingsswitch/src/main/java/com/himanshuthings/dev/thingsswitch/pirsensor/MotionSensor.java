package com.himanshuthings.dev.thingsswitch.pirsensor;

/**
 * Created by himanshubaghel on 08/12/17.
 */

public interface MotionSensor {
    void startup();

    void shutdown();

    interface Listener {
        void onMovement();
    }
}
