package com.himanshuthings.dev.switchcontrol;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * Created by Himanshu on 23-Jun-17.
 */

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private ArrayList<Switch> mSwitches = new ArrayList<>();
    private SwitchesAdapter mAdapter;

    private DatabaseReference mDatabaseRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Set the BCM6 port as the GPIO for all pins.
        mSwitches = Switch.getSwitches(getApplicationContext());

        mDatabaseRef = FirebaseDatabase.getInstance().getReference().child(Constant.REF_NAME);

        mDatabaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (Switch s : mSwitches) {
                    Switch dataSwitch = dataSnapshot.child(s.getName()).getValue(Switch.class);
                    Log.i(TAG,dataSwitch.getName()+":"+dataSwitch.getStatus());
                    //Check changed pin by the pin name
                    if (s.getName().equals(dataSwitch.getName())) {
                        s.setStatus(dataSwitch.getStatus());
                    }
                }

                //Refresh the list
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

//Set recycler view
        mAdapter = new SwitchesAdapter(this, mSwitches, mDatabaseRef);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.switches_list);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setAdapter(mAdapter);
    }
}
