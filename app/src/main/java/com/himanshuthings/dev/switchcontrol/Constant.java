package com.himanshuthings.dev.switchcontrol;

/**
 * Created by Himanshu on 23-Jun-17.
 */

public class Constant {
    public static final String REF_NAME = "switchlist";

    //Name of the different pins.
    public static final String LIGHT_BULB_PIN = "lightBulbPin";
}
